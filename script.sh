echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

# install cURL
sudo apt-get -y install curl

# install oracle java 
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get -y install oracle-java8-installer

# install npm
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential

# install git
sudo apt-get update
sudo apt-get -y install git

# install js toolsets
npm install -g yo
npm install -g bower
npm install -g gulp
npm install -g generator-jhipster
