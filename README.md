# README #

Vagrantfile contains the necessary information regarding the configuration of a vm which serves as the execution environment of the application. It also makes use of script.sh which is executed automatically within the vm.

script.sh contains the necessary steps which are executed automatically by vagrant and install all necessary software which make up the toolset for the development environment.